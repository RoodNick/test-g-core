import { TestGCorePage } from './app.po';

describe('test-g-core App', () => {
  let page: TestGCorePage;

  beforeEach(() => {
    page = new TestGCorePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
